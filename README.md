# Klarna Stortodynamodb

this is used to store data to dynamodb

## Getting started

* Create an ECR klarna-stortodynamodb. 
* Clone the repo
    ```
    Git clone https://gitlab.com/klarnacodetest/klarna-stortodynamodb.git
    ```
* Go inside the project and build the docker image.
    ```
    cd klarna-stortodynamodb
    docker build -t  klarna-stortodynamodb .
    docker tag klarna-stortodynamodb:latest <-aws-accountid->.dkr.ecr.us-east-1.amazonaws.com/klarna-stortodynamodb:latest
    docker push <-aws account -d->.dkr.ecr.us-east-1.amazonaws.com/klarna-stortodynamodb:latest
    ```
* Create a Dynamodb table called klarna_campain_data.
    - Create the lambda function klarna-stortodynamodb
    - Create an IAM for lambda function that should have "dynamodb:BatchWriteItem" access. You can find the IAM policy from permission dir in repo.
    - Navigate to the lambda section in aws console.
    - Click the button create function in the top right corner.
    - Now choose the container image option and provide the name for the lambda function in this case name is klarna-stortodynamodb.
    - Choose the upload docker image in the above step.
    - Choose the IAM role which was created.
    - Then create the lambda function.
    - Create a trigger for S3 bucket (Klarna-csvuploader) which we have created on Klarna-csvuploader, this will trigger the lambda function when the new csv is    uploaded to the bucket.
