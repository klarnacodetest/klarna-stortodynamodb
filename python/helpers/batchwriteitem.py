from asyncio.log import logger
import boto3


class BatchWriteItem:

    def batch_write(connection, table, items):

        try:

            response = connection.batch_write_item(
                RequestItems={
                    table: items,
                },
            )

        except KeyError as e:
            logger.info(e)
