import pandas

from asyncio.log import logger


class Readcsv:

    def readcsv(file_path, chunck_size):

        try:
            chunck = pandas.read_csv(file_path, chunksize=chunck_size)
            return chunck
        except FileNotFoundError as e:
            logger.info(e)
