''' 
author: uyanushka@gmail.com 
main python file use to handle the app
'''
import json
import pandas
import yaml
import os
from email import parser
from asyncio.log import logger
from yaml import parse
from helpers.batchwriteitem import BatchWriteItem
from helpers.boto3connection import Boto3connection
from helpers.readcsv import Readcsv
from helpers.s3download import S3GetObject


def orchestrate_campain_data(campain_data, campain_id):
    '''
    Task orchestrate data read by csv and format to store in dynamodb
    parameters
    -----------------------
    campain_data: csv file data
    campain_id: campain id
c
    '''

    items = []

    data_frame = pandas.DataFrame(campain_data)
    data_frame = data_frame.reset_index()

    for index, row in data_frame.iterrows():

        PutRequest = {'PutRequest': {'Item': {}}}

        PutRequest["PutRequest"]["Item"] = {"campain_id": {'S': campain_id}, "user_id": {
            'S': str(row['user_id'])}, "promo_code": {'S': str(row['promo_code'])}}

        items.append(PutRequest)

    return items


def read_config(file_path):
    ''' 
    Task read the config file and return values
    parameters
    ----------
    file_path: file path which is need to read
    '''
    with open(file_path) as stream:
        try:
            data = yaml.safe_load(stream)
            return data

        except yaml.YAMLError as e:
            logger.info(e)


def s3_get_file(client, bucket, key, file_type):
    '''
    Task get object from s3 bucket based on the data format
    parameters
    ----------
    client: s3 client
    bucket: s3 bucket name
    key: file sub path
    file_type : json or csv
    '''
    if file_type == "csv":

        responce = S3GetObject.s3_get_object(
            client, bucket, key)

        try:
            return responce
        except Exception as e:
            logger.info(e)
    else:
        responce = S3GetObject.s3_get_object(
            client, bucket, key)
        try:
            data = json.loads(responce.read())
            return(data)

        except Exception as e:
            logger.info(e)


def lambda_handler(event, context):
    '''
    Lambda handler manage the api gateway parameters in headers and body.
    '''

    confgs = read_config("configs/config.yml")

    chunk_size = confgs["chunk_size"]
    region = confgs["region"]
    dynamodb_table = confgs["dynamodb_table"]
    s3_bucket = confgs["s3_bucket"]
    static_config = confgs["static_config"]
    static_config_json = static_config+".json"

    s3client = Boto3connection.get_connection(region, 's3')

    campain_confgs = s3_get_file(
        s3client, s3_bucket, static_config_json, "json")

    s3_bucket = campain_confgs["s3_bucket"]
    campain_id = campain_confgs["campain_id"]
    config_path = campain_confgs["config_path"]

    campain_info = s3_get_file(s3client, s3_bucket, config_path, "csv")

    campain_info = Readcsv.readcsv(campain_info, chunk_size)
    dynamodb = Boto3connection.get_connection(region, 'dynamodb')

    for campain_data in campain_info:

        items = orchestrate_campain_data(campain_data, campain_id)

        BatchWriteItem.batch_write(dynamodb, dynamodb_table, items)
